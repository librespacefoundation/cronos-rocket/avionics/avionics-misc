# Avionics Misc

The Avionics Misc repository mainly helps us organize generic lab tasks. Anything that is not directly related to any of our main boards, as well as Harnesses work and collateral PCBs is contained in this repository.    